Package: bigleaf
Type: Package
Version: 0.7.6
Date: 2022-01-18
Title: Physical and Physiological Ecosystem Properties from Eddy Covariance Data
Authors@R: c( person("Juergen", "Knauer", role=c("aut","cre"), email="J.Knauer@westernsydney.edu.au"),
              person("Thomas", "Wutzler", role=c("aut"), email="twutz@bgc-jena.mpg.de"),
              person("Soenke", "Zaehle", role=c("ctb"), email="szaehle@bgc-jena.mpg.de"),
			        person("Tarek", "El-Madany", role=c("ctb"), email="telmad@bgc-jena.mpg.de"),
			        person("Mirco", "Migliavacca", role=c("ctb"), email="mmiglia@bgc-jena.mpg.de")
			      )	
Maintainer: Juergen Knauer <J.Knauer@westernsydney.edu.au>
Description: Calculation of physical (e.g. aerodynamic conductance, surface temperature), 
             and physiological (e.g. canopy conductance, water-use efficiency) ecosystem properties
			 from eddy covariance data and accompanying meteorological measurements. Calculations
			 assume the land surface to behave like a 'big-leaf' and return bulk ecosystem/canopy variables.
URL: https://bitbucket.org/juergenknauer/bigleaf
BugReports: https://bitbucket.org/juergenknauer/bigleaf/issues
Depends: R (>= 2.10)
Imports: robustbase, solartime
License: GPL (>= 2)
Encoding: UTF-8
LazyData: yes
RoxygenNote: 7.1.2
Suggests: knitr,
    rmarkdown, testthat
VignetteBuilder: knitr
